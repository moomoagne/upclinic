import { Routes } from '@angular/router';
import {AccueilComponent} from './accueil/accueil.component';
import {ContactComponent} from './contact/contact.component';
import {DocteurComponent} from './docteur/docteur.component';
import {NosServicesComponent} from './nos-services/nos-services.component';

export const AppRoute: Routes = [
    {path : 'accueil', component: AccueilComponent},
    {path : '', redirectTo: '/accueil', pathMatch: 'full'},
    {path : 'contact', component: ContactComponent},
    {path : 'docteur', component: DocteurComponent},
    {path : 'rendez-vous', component: NosServicesComponent}
];
