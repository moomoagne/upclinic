import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-docteur',
  templateUrl: './docteur.component.html',
  styleUrls: ['./docteur.component.css']
})
export class DocteurComponent implements OnInit {
    private nom: string;
    private prenom: string;
    private experience: number;
    private age: number;
    private adresse: string;
    private specialite: string;
    private email_medecin: string;
    private jr_de_travail: string;
    private description: string;
  constructor() {
     this.nom = this.nom = 'AGNE';
     this.prenom = this.prenom = 'Mamadou';
     this.experience = this.experience = 10;
     this.age = this.age = 32;
     this.adresse = this.adresse = 'Cite Aire Afrique Oeust Foire';
     this.specialite = this.specialite = 'Cardiologie';
     this.email_medecin = this.email_medecin = 'moomoagne@gmail.com';
     this.jr_de_travail = this.jr_de_travail = 'Lundi au vendredi';
     this.description = this.description = 'Lorem';
  }
  ngOnInit() {
  }

}
