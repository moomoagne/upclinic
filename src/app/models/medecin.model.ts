export interface MedecinModel {
    id: number;
    nom: string;
    prenom: string;
    adresse: string;
    specialite: string;
    jr_de_travail: any;
    age: number;
    email: string;
    experience: number;
    description: string;
}
