import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoute } from './app.routes';

import { AppComponent } from './app.component';
import {HeaderComponent} from './accueil/header.component';
import {AccueilComponent} from './accueil/accueil.component';
import {FooterComponent} from './accueil/footer.component';
import {ListDocteurComponent} from './accueil/list-docteur.component';
import {SliderComponent} from './accueil/slider.component';
import { FormsModule } from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';
import { ContactComponent } from './contact/contact.component';
import { DocteurComponent } from './docteur/docteur.component';
import { NosServicesComponent } from './nos-services/nos-services.component';

@NgModule({
  declarations: [
    AppComponent,
    AccueilComponent,
    ListDocteurComponent,
    HeaderComponent,
    FooterComponent,
    SliderComponent,
    ContactComponent,
    DocteurComponent,
    NosServicesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(AppRoute, { enableTracing: true } ),
    RouterModule.forChild(AppRoute),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
